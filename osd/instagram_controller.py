import requests
import os
import json
import re
import sys
import urllib
import time
from datetime import datetime, timedelta
import mysql_controller as sql
import instaloader
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Insta:
    def __init__(self,username):
        self.username = username
        self.token = 0
        self.profilePageNumber = 0
    
    def pageRequest(self):
        """Visit the profile page of the Instagram user. Fetch the profile page number"""
        self.fetchPage = requests.get("https://instagram.com/"+self.username)
        if self.fetchPage.status_code == 200:
            self.token = 1
            temp = "\n".join(re.findall(r'profilePage_\d+',self.fetchPage.text))
            self.profilePageNumber = re.sub(r"\D","",temp)
            return self.token
        elif self.fetchPage.status_code == 404:
            self.token = 0
            print(bcolors.FAIL + "Profile doesn't exist." + bcolors.ENDC)
            return self.token
        else:
            self.token = 0
            print("Something Else Fucked Up")
            return self.token

    def fetchJSONResponse(self):
        """Fetch the JSON response from the instagram API"""
        self.jsonResponse = None
        if self.token == 1:
            contentData = re.findall(r'<script type="text/javascript">window[.]_sharedData = {[\s\S]*};</script>',self.fetchPage.text)
            jsonData = contentData[0].replace('<script type="text/javascript">window._sharedData = ','')
            jsonData = jsonData.replace(';</script>', '')
            self.jsonResponse = json.loads(jsonData)['entry_data']['ProfilePage'][0]
            return self.jsonResponse
        else:
            print("Please check. There seems to be problem")
            pass
            
    def makeDirectory(self):
        """Make directory for media and user if it doesn't exist already"""
        if self.jsonResponse:
            self.directoryPath = os.path.dirname(os.path.abspath(__file__)) + "/media/" + self.jsonResponse["graphql"]["user"]["username"]
            mediaPath = os.path.dirname(os.path.abspath(__file__)) + "/media/"
            if self.token == 1:
                if os.path.isdir(mediaPath):
                    pass
                elif not os.path.isdir(mediaPath):
                    os.mkdir(mediaPath)
                if os.path.isdir(self.directoryPath):
                    return self.directoryPath
                elif not os.path.isdir(self.directoryPath):
                    os.mkdir(self.directoryPath)
                    return self.directoryPath
            else:
                pass
        if not self.jsonResponse:
            pass

    def fetchProfilePicture(self):
        """Downloads Profile Picture of the account. Skips if already exists."""
        if self.token == 1:
            self.profilePicture = self.jsonResponse["graphql"]["user"]["profile_pic_url_hd"]
            os.system("wget -nc " + self.jsonResponse["graphql"]["user"]["profile_pic_url_hd"] + " -P " + self.directoryPath)
            return 0
        else:
            return 1

    def accountStatus(self):
        """Check the Account Status Private/Public"""
        self.tokenAccountStatus = 1
        if self.profilePageNumber:
            if self.jsonResponse["graphql"]["user"]["is_private"] == True:
                print(bcolors.WARNING + "Its private. Nothing to do here.." + bcolors.ENDC)
                self.tokenAccountStatus = 1
                return self.tokenAccountStatus
            elif self.jsonResponse["graphql"]["user"]["is_private"] == False:
                self.tokenAccountStatus = 0
                print(bcolors.OKGREEN + "BINGO !. Open Account." + bcolors.ENDC)
                return self.tokenAccountStatus
        else:
            return 1
    
    def extensionControl(self,ext):
        """This is an extension control that helps in keeping files with correct extensions."""
        try:
            for filename in os.listdir(self.directoryPath):
                newName = re.sub(r"(.jpg?.+$)","."+ext,filename)
                os.rename(self.directoryPath + "/" + filename,self.directoryPath + "/" + newName)
                #print(newName)
        except FileExistsError:
            pass
    
    def instaInformationControl(self, func):
        """Inserts information to control block of the database."""
        self.fetchMediaCount()
        idInstance = sql.instagramID(self.username, self.media_count)
        if func == "add":
            idInstance.addUserInstagram()
        elif func == "rem":
            idInstance.removeUser()

    def fetchMediaCount(self):
        if self.tokenAccountStatus == 0:
            self.media_count = self.jsonResponse["graphql"]["user"]["edge_owner_to_timeline_media"]["count"]
            
    def instaMediaDownloader(self, timeLast = 0):
        """Download media from the instagram account"""
        photoLinks, videoLinks, photoDates, videoDates, photoCaptions, videoCaptions = [], [], [], [], [], []
        L = instaloader.Instaloader()
        posts = instaloader.Profile.from_username(L.context, self.username).get_posts()
        hourBefore = datetime.now() - timedelta(hours = 1)
        if timeLast == 0:
            timeLast = hourBefore
        for post in posts:
            if str(post.date_local) > str(timeLast):
                if post.typename == "GraphSidecar":
                    for sidecar_node in post.get_sidecar_nodes():
                        if not sidecar_node.is_video:
                            photoLinks.append(sidecar_node.display_url)
                            photoDates.append(post.date_local)
                            photoCaptions.append(post.caption)
                        elif sidecar_node.is_video:
                            videoLinks.append(sidecar_node.video_url)
                            videoDates.append(post.date_local)
                            videoCaptions.append(post.caption)
                elif post.typename == 'GraphImage':
                    photoLinks.append(post.url)
                    photoDates.append(post.date_local)
                    photoCaptions.append(post.caption)
                elif post.typename == 'GraphVideo':
                    videoLinks.append(post.video_url)
                    videoDates.append(post.date_local)
                    videoCaptions.append(post.caption)
                else:
                    print("Error. Type: Unknown")
                    return
            else:
                break
        return photoLinks, videoLinks, photoDates, videoDates, photoCaptions, videoCaptions