import os
import random
import telepot
from telepot.loop import MessageLoop
#from pprint import pprint
import requests
import time
import datetime
from datetime import date
import instagram_controller as instagram
import mysql_controller as sql


TOKEN = os.environ.get("TELEGRAM_KEY")
my_chat = os.environ.get("MY_CHAT")
cChat_id = os.environ.get("TELEGRAM_CHANNEL")
print(my_chat)

def handle(msg):
    #pprint(msg)
    global content_type, chat_type, chat_id, first_name, last_name, username
    content_type, chat_type, chat_id = telepot.glance(msg)
    #print(content_type, chat_id, chat_type)
    
    if chat_type == "channel":
        channelControl(msg)
    else:
        first_name = msg["chat"]["first_name"]
        if "last_name" not in msg["chat"]:
            last_name = msg["chat"]["last_name"]
        else:
            last_name = "none "
        if "username" not in msg["chat"]:
            username = "none "
        else:
            username = msg["chat"]["username"]
        print(chat_id, username)
        commandController(msg)

def channelControl(msg):
    pass

def commandController(msg):
    downloadFlag = False
    
    userInstance = sql.telegramUsers(chat_id, username)
    adminInstance = sql.telegramAdmin(chat_id, username)
    if adminInstance.isUserBanned():
        bot.sendMessage(chat_id,"Nothing for you.")
        return

    if content_type == "text":
        userQuery = msg["text"]
        logger(userQuery)
        print (userQuery)
        if "/start" in userQuery:
            startMsg(chat_id)
        elif userQuery == "lodalassun":
            if userInstance.checkUser():
                sendGif(chat_id,4)
            else:
                userInstance.addTelegramUser()
                sendGif(chat_id,1)
        elif userQuery == "makemedaddy":
            if adminInstance.isAdmin():
                bot.sendMessage(chat_id,"Already admin.")
            else:
                adminInstance.makeDaddy()
                bot.sendMessage(chat_id,"Promoted to admin.")
        elif "/info" in userQuery:
            readInfo(chat_id)
        elif userQuery[0] == "/":
            if userInstance.checkUser():
                try:
                    cmd, query = userQuery.split(" ",1)
                except ValueError:
                    sendGif(chat_id,4)
                    return
                instaInstance = instagram.Insta(query)
                if "/ig" in cmd:
                    print(query)
                    if cmd == "/igd":
                        downloadFlag = True
                    if instaInstance.pageRequest() == 1:
                        instaInstance.fetchJSONResponse()
                        account_status = instaInstance.accountStatus()
                        botResponse = query
                        if cmd == "/iga" or cmd == "/igd":
                            if account_status == 0:
                                if downloadFlag:
                                    instaInstance.makeDirectory()
                                    instaInstance.fetchProfilePicture()
                                    instaInstance.extensionControl("jpg")
                                    postBot(query, chat_id)
                                else:
                                    instaInstance.instaInformationControl("add")
                                    botResponse = "Tracking "+query
                            else:
                                sendGif(chat_id,2)
                        elif cmd == "/igr":
                            instaInstance.instaInformationControl("rem")
                            botResponse = query + " removed"
                        bot.sendPhoto(chat_id,instaInstance.jsonResponse["graphql"]["user"]["profile_pic_url_hd"],caption=botResponse)
                    else:
                        bot.sendMessage(chat_id, "Profile doesn't exist!")
                elif cmd == "/sub":
                    chatInstance = sql.telegramChat(chat_id, username, first_name, last_name)
                    chatInstance.addTelegramChat()
                    bot.sendMessage(chat_id, "Subscribed.")    
                elif cmd == "/ban":
                    if adminInstance.isAdmin():
                        adminInstance.banUser(query)
                        bot.sendMessage(chat_id, "User banned.")
                    else:
                        bot.sendMessage(chat_id, "U no admin. -_-")
                else:
                    sendGif(chat_id,4)
            else:
                sendGif(chat_id,0)
        else:
            sendGif(chat_id,4)
    else:
        sendGif(chat_id,0)

def startMsg(chat_id):
    sendGif(chat_id,3)

def readInfo(chat_id):
    bot.sendMessage(chat_id,"Use /ig <username> to get Profile picture. \n Use Use /igd <username> to download posts. \n Use /sub subscribe. \n Use /iga <username> to add instagram user.")
    
def sendGif(chat_id, i):
    """Sends reply in GIFs. I hope this site never closes. 
    0 for  who_the_fuck_is_you_gifs
    1 for successful_authentication.
    2 Admin problems
    3 Wazzzuppp.
    """
    # Unknown people gets this message
    who_the_fuck_is_you_gifs = ["https://media.giphy.com/media/l41YqkbFs0EtgE0g0/giphy.gif","https://media.giphy.com/media/x1kS7NRIcIigU/giphy.gif","https://media.giphy.com/media/3o6ZtcaQX0Xa8FPLX2/giphy.gif","https://media.giphy.com/media/8qrcxR55ZEBQRdEAz1/giphy.gif","https://media.giphy.com/media/3o7WIxyAMnXChimmXe/giphy.gif","https://media.giphy.com/media/9MIlCEyZ5hytszJV9g/giphy.gif","https://media.giphy.com/media/3o72EVymX8u70s22mQ/giphy.gif","https://media.giphy.com/media/10vXSmTzvg25Yk/giphy.gif","https://media.giphy.com/media/l0HlThB0f56Jun1XG/giphy.gif","https://media.giphy.com/media/co7KFI57yaXIY/giphy.gif","https://media.giphy.com/media/fnuSiwXMTV3zmYDf6k/giphy.gif","https://media.giphy.com/media/xUySTIOsf7QxHx1gk0/giphy.gif"]
    
    # Upon successful authentication
    successful_authentication = ["https://media.giphy.com/media/3oEjI8vHniF9LGQ0JW/giphy.gif","https://media.giphy.com/media/3o7TKF1fSIs1R19B8k/giphy.gif"]
    
    # Admin mistakes
    what_are_you_smoking = ["https://media.giphy.com/media/ftdixaBdZwgut0AITr/giphy.gif","https://media.giphy.com/media/26gJAaPZKuQutL1mg/giphy.gif"]
    you_idiot = ["https://media.giphy.com/media/W78gPFDEvXhi8/giphy.gif","https://media.giphy.com/media/13k0uUW3Aicx20/giphy.gif","https://media.giphy.com/media/hX3gBVusgQwM0/giphy.gif","https://media.giphy.com/media/126omQ0GgfVVmw/giphy.gif","https://media.giphy.com/media/4DA5qIUV1w2oU/giphy.gif","https://media.giphy.com/media/DpPteKmdu3CnK/giphy.gif"]
    
    # Say Hello
    say_hello = ["https://media.giphy.com/media/10a9ikXNvR9MXe/giphy.gif", "https://media.giphy.com/media/9HBduC3ZIgrG8/giphy.gif", "https://media.giphy.com/media/3oEjHQn7PBRvy9A5mE/giphy.gif", "https://media.giphy.com/media/CoZR5jasUDp3q/giphy.gif", "https://media.giphy.com/media/F9xSlEGUPObba/giphy.gif", "https://media.giphy.com/media/6yU7IF9L3950A/giphy.gif", "https://media.giphy.com/media/eoVusT7Pi9ODe/giphy.gif", "https://media.giphy.com/media/l0MYC0LajbaPoEADu/giphy.gif", "https://media.giphy.com/media/FQyQEYd0KlYQ/giphy.gif","https://media.giphy.com/media/l46Cpz0A0dB1jMxG0/giphy.gif","https://media.giphy.com/media/4ZohQ6LXAhGL0OZ6k1/giphy.gif","https://media.giphy.com/media/5hfinCirdgvzwlqaWU/giphy.gif","https://media.giphy.com/media/26Ffk99hs5WtggmIw/giphy.gif"]

    if i == 0:
        randomInt = random.randint(0,len(who_the_fuck_is_you_gifs)-1)
        bot.sendMessage(chat_id, who_the_fuck_is_you_gifs[randomInt])
    
    elif i == 1:
        randomInt = random.randint(0,len(successful_authentication)-1)
        bot.sendMessage(chat_id,successful_authentication[randomInt] + "\n \n Welcome")
    
    elif i == 2:
        randomInt = random.randint(0,len(you_idiot)-1)
        bot.sendMessage(chat_id,you_idiot[randomInt] + " \n \n You retard. This account is private, don't waste my fucking bandwidth. Use /ig")

    elif i == 3:
        randomInt = random.randint(0, len(say_hello)-1)
        bot.sendMessage(chat_id, say_hello[randomInt] + " \n \n Use /info")

    else:
        randomInt = random.randint(0,len(what_are_you_smoking)-1)
        bot.sendMessage(chat_id,what_are_you_smoking[randomInt] + " \n \n What are you smoking ?")

def logger(query):
    dateTime = datetime.datetime.now()
    today = date.today()
    folderPath = os.getcwd()
    logPath = folderPath+"/Log/"+str(today)
    if os.path.isdir(logPath):
        pass
    elif not os.path.isdir(logPath):
        os.makedirs(logPath)
    
    data = str(dateTime)+"\t"+str(chat_id)+", "+first_name+", "+username+", "+query+"\n"
    file = open(os.path.join(logPath,str(chat_id)+".txt"), "a")
    file.write(data)
    file.close()

def dbControl():
    mediaInstance = sql.mediaAccess()
    usernames = mediaInstance.getUsernames()
    if os.path.exists("downloaderFile.txt"):
        file = open("downloaderFile.txt", "r+")
        timeLast = file.read()
    else:
        file = open("downloaderFile.txt", "w+")
        timeLast = 0
    timeNow = datetime.datetime.now()
    file.seek(0)
    if usernames:
        for userName in usernames:
            postBot(userName, cChat_id, timeLast)
    file.write(str(timeNow))
    file.close()

def postBot(userName, cChat_id, timeLast = 0):
    print(userName)
    instaInstance = instagram.Insta(userName)
    photoLinks, videoLinks, photoDates, videoDates, photoCaptions, videoCaptions = instaInstance.instaMediaDownloader(timeLast)
    if not photoLinks and not videoLinks:
        if not cChat_id:
            bot.sendMessage(cChat_id, "No new post in last hour.")
    else:
        if photoLinks:
            for (link, date, cap) in zip(photoLinks, photoDates, photoCaptions):
                if cap == None or len(cap) > 1024:
                    cap = ""
                    bot.sendPhoto(cChat_id,link,caption=userName+"\n"+cap+"\n"+str(date))
                else:
                    bot.sendPhoto(cChat_id,link,caption=userName+"\n"+cap+"\n"+str(date))
        if videoLinks:
            for (link, date, cap) in zip(videoLinks, videoDates, videoCaptions):
                if cap == None or len(cap) > 1024:
                    cap = ""
                    bot.sendVideo(cChat_id,link,caption=userName+"\n"+cap+"\n"+str(date))
                else:
                    bot.sendVideo(cChat_id,link,caption=userName+"\n"+cap+"\n"+str(date))

bot = telepot.Bot(TOKEN)
MessageLoop(bot,handle).run_as_thread()
print ("Listening")

while True:
    dbControl()
    time.sleep(3600)
    
