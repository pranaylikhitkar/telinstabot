import sqlite3
import credentials

# config = {'host' : credentials.host, 'user' : credentials.user, 'password' : credentials.password, 'database' : credentials.database}
# botDatabase = mysql.connect(**config)
# dbCursor = botDatabase.cursor()

botDatabase = sqlite3.connect('teleinsta.db',check_same_thread=False)
dbCursor = botDatabase.cursor()

class telegramAdmin:
    def __init__(self, chat_id, username):
        self.chat_id = chat_id
        self.username = username
        self.admin_status = True
        dbCursor.execute('''SELECT name FROM sqlite_master WHERE type="table" AND name="telegram_admin"''')
        if dbCursor.fetchone()[0]==1 : 
            print('Table exists.')


    def makeDaddy(self):
        if self.isAdmin() == False:
            query = "INSERT INTO telegram_admin (chat_id, userName, admin_status) VALUES (?, ?, ?)"
            values = (self.chat_id, self.username, self.admin_status)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record inserted")
        else:
            pass

    def isAdmin(self):
        result = False
        query = "SELECT chat_id FROM telegram_admin WHERE chat_id = ?"
        values = (self.chat_id,)
        dbCursor.execute(query, values)
        record = dbCursor.fetchall()
        if record:
            result = True
        return result

    def banUser(self, user_id):
        query = "INSERT INTO black_list (chat_id) VALUES (?)"
        values = (user_id,)
        dbCursor.execute(query, values)
        botDatabase.commit()
        print(dbCursor.rowcount, "record inserted")
    
    def isUserBanned(self):
        result = False
        query = "SELECT chat_id FROM black_list WHERE chat_id = ?"
        values = (self.chat_id,)
        dbCursor.execute(query, values)
        record = dbCursor.fetchall()
        if record:
            result = True
        return result


class telegramUsers:
    def __init__(self, chat_id, username):
        self.chat_id = chat_id
        self.username = username

    def addTelegramUser(self):
        if self.checkUser() == False:
            query = "INSERT INTO telegramUsers (chat_id, userName) VALUES (?, ?)"
            values = (self.chat_id, self.username)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record inserted")
        else:
            return "exist"

    def checkUser(self):
        result = False
        query = "SELECT chat_id FROM telegramUsers WHERE chat_id = ?"
        values = (self.chat_id,)
        dbCursor.execute(query, values)
        record = dbCursor.fetchall()
        if record:
            result = True
        return result

class telegramChat:
    def __init__(self,chat_id, username, first_name, last_name):
        self.chat_id = chat_id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.subscription_status = True

    def addTelegramChat(self):
        if self.checkUser() == False:
            query = "INSERT INTO telegram_chat (chat_id, username, first_name, last_name, subscription_status) VALUES (?, ?, ?, ?, ?)"
            values = (self.chat_id, self.username, self.first_name, self.last_name, self.subscription_status)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record inserted")
        else:
            return "exist"
    
    def checkUser(self):
        result = False
        query = "SELECT chat_id FROM telegram_chat WHERE chat_id = ?"
        values = (self.chat_id,)
        dbCursor.execute(query, values)
        record = dbCursor.fetchall()
        if record:
            result = True
        return result

    
class instagramID:
    def __init__(self, username, media_count):
        self.username = username
        self.media_count = media_count
        
    def checkUser(self):
        result = False
        query = "SELECT username FROM instagram_id WHERE username = ?"
        values = (self.username,)
        dbCursor.execute(query, values)
        record = dbCursor.fetchall()
        if record:
            result = True
        return result
    
    def addUserInstagram(self):
        if not self.checkUser():
            query = "INSERT INTO instagram_id(username, media_count) VALUES (?, ?)"
            values = (self.username, self.media_count)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record inserted")
        else:
            query = "UPDATE instagram_id SET media_count = ? WHERE username = ?"
            values = (self.media_count, self.username,)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record inserted")
        
    def removeUser(self):
        if self.checkUser():
            query = "DELETE FROM instagram_id WHERE username = ?"
            values = (self.username,)
            dbCursor.execute(query, values)
            botDatabase.commit()
            print(dbCursor.rowcount, "record deleted")
        else:
            print("no")

class mediaAccess:
    def getUsernames(self):
        usernames = []
        query = "SELECT username FROM instagram_id"
        dbCursor.execute(query)
        records = dbCursor.fetchall()
        if records:
            for record in records:
                usernames.append(record[0])
        else:
            print("no record")
        return usernames
    