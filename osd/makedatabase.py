import sqlite3

conn = sqlite3.connect("teleinsta.db")
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS telegramUsers(chat_id varchar(20), username varchar(30))''')
c.execute ('''CREATE TABLE IF NOT EXISTS instagram_id(username varchar(50), media_count int)''')
c.execute('''CREATE TABLE IF NOT EXISTS telegram_admin(chat_id varchar(50), username varchar(50), admin_status bool)''')
c.execute('''CREATE TABLE IF NOT EXISTS telegram_chat(chat_id varchar(50), username varchar(50), first_name varchar(50))''')
c.execute('''CREATE TABLE IF NOT EXISTS black_list(chat_id varchar(50))''')
#c.execute('''
conn.commit()